<?php

namespace Drupal\views_flipster\Plugin\views\style;

use Drupal\views\Plugin\views\style\StylePluginBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Style plugin to render each item into flipster carousel.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "flipster",
 *   title = @Translation("Flipster"),
 *   help = @Translation("Displays rows in Flipster Styles."),
 *   theme = "views_flipster_views",
 *   display_types = {"normal"}
 * )
 */
class ViewsFlipster extends StylePluginBase {

  /**
   * Does the style plugin allows to use style plugins.
   *
   * @var bool
   */
  protected $usesRowPlugin = TRUE;

  /**
   * Does the style plugin support custom css class for the rows.
   *
   * @var bool
   */
  protected $usesRowClass = TRUE;

  /**
   * Builds the configuration form.
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['style'] = array(
      '#type' => 'select',
      '#title' => $this->t('Flipster Style'),
      '#description' => $this->t('Choose your style.'),
      '#default_value' => $this->options['style'],
      '#options' => array(
        'coverflow' => $this->t('Coverflow'),
        'carousel' => $this->t('Carousel'),
        'wheel' => $this->t('Wheel'),
        'flat' => $this->t('Flat'),
      ),
    );
  }

  /**
   * Defines default value.
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['style'] = array('default' => 'coverflow');
    return $options;
  }

}
