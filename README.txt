CONTENTS OF THIS FILE
---------------------
   
 * INTRODUCTION
 * FEATURES
 * REQUIREMENTS
 * INSTALLATION
 * USAGE
 * MAINTAINERS
 * DEMO

INTRODUCTION
------------

Views Flipster module integrates the jQuery.Flipster library 
(https://github.com/drien/jquery-flipster) with Drupal Views module to 
replicate gorgeous Flipster effects. 

FEATURES
--------

This CSS3 3D transform-based jQuery plugin is:

  - Responsive
  - Lightweight
  - Touch/Scrollwheel/Trackpad/Keyboard Friendly
  - Flexible
  - Customizable

REQUIREMENTS
------------

Views Flipster module requires:

1. Download jQuery Flipster library - https://github.com/drien/jquery-flipster
2. Enable Drupal 8 core - Field UI module
3. Enable Drupal 8 core - Image module
4. Enable Drupal 8 core - Views, Views UI modules
5. Enable Drupal 8 contributed - Libraries API module

INSTALLATION
------------

1. Download the 'Views Flipster' module archive. Extract and place it in the 
root modules directory i.e. /modules
2. Create a libraries directory in the root if not already there i.e. /libraries
3. Extract and rename the downloaded jQuery Flipster library to "flipster" 
(pay attention to case of the letters)
4. Place the extracted flipster directory in the libraries directory 
i.e. /libraries/flipster
5. Ensure you have similar path for these below files:
     - /libraries/flipster/jquery.flipster.min.css
     - /libraries/flipster/jquery.flipster.min.js
6. Enable the 'Views Flipster' module

USAGE
-----

1. Create a new Content Type with an image field (set max of 1 image 
upload per node)
2. Add some nodes of this type with their images
3. Create a new view of this content type. Add the image field you created 
earlier
4. Select the Views Format as 'Flipster'. In Settings, default style is set 
to 'Coverflow'. You have 3 more options besides it  - Carousel, Wheel and, Flat
5. Try each one of the four styles for your visual delight

MAINTAINERS
-----------

Current maintainer:

 * Manoj Kulkarni - https://www.drupal.org/user/3506687
 
DEMO
----

http://brokensquare.com/Code/jquery-flipster/demo
