(function ($, Drupal, drupalSettings) {
  'use strict';
  Drupal.behaviors.flipster = {
    attach: function (context, settings) {
      if ($('#views_flipster').hasClass('carousel')) {
        $('#views_flipster').flipster(
          {
            style: 'carousel',
            spacing: -0.5,
            nav: true,
            buttons: true
          }
        );
      }
      else if ($('#views_flipster').hasClass('wheel')) {
        $('#views_flipster').flipster(
          {
            style: 'wheel',
            spacing: 0
          }
        );
      }
      else if ($('#views_flipster').hasClass('flat')) {
        $('#views_flipster').flipster(
          {
            style: 'flat',
            spacing: -0.25
          }
        );
      }
      else {
        $('#views_flipster').flipster();
      }
    }
  };
})(jQuery, Drupal, drupalSettings);
